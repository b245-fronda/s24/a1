// console.log("Hello world");

let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

let address = ["123", "Main Street", "Brgy Poblacion", "Manila", "Metro Manila", "4321"];
let [houseNumber, street, barangay, municipality, province, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${barangay} ${municipality}, ${province} ${zipCode}`);

let animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	measurement: {
		ft: 20,
		inch: 3
	}
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed ${weight} kgs with a measurement of ${measurement.ft} ft ${measurement.inch} in.`);

let numbers = [1, 2, 3, 4, 5];
numbers.forEach(number => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);

class Dog{
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);